import React, { Component } from 'react';
// import { Constants } from 'expo';
import { NativeRouter, Route, Switch } from 'react-router-native'; // 4.2.0
import { View, Text, StyleSheet } from 'react-native';
// You can import from local files
import MainScreen from './screens/main';
import DefaultScreen from './screens/404';
import Navigation from './components/navigation'
import Transitions from './components/transitions'

const ColoredScreen = ({ color }) => (
  <View style={{...StyleSheet.absoluteFillObject, backgroundColor: 'white', opacity: 1 }}>
    <Navigation />
    <View style={{ flex: 1, backgroundColor: color, opacity: 0.7 }} />
  </View>
)

const ColoredRoute = ({ color }) => (
  <Route path={`/${color}`} render={() => <ColoredScreen color={color} />} />
)

export default class App extends Component {
  render() {
    return (
        <NativeRouter>
          <Route children={(routeProps) => (
            <Transitions {...routeProps}>
              <Switch
                // without this, transitions wont work properly
                location={routeProps.location}
              >
                <Route exact path="/" component={MainScreen} />
                <Route path="/blue" render={() => <ColoredScreen color="blue" />} />
                <Route path="/orange" render={() => <ColoredScreen color="orange" />} />
                <Route path="/pink" render={() => <ColoredScreen color="pink" />} />
                <Route component={DefaultScreen} />

              </Switch>
            </Transitions>
          )}/>
        </NativeRouter>
    );
  }
}
