import React from 'react'
import { View, Animated, StyleSheet, Dimensions, Easing } from 'react-native'
import PropTypes from 'prop-types'


const TransitionLayer = ({ children, animatePosition, width }) => (
    <Animated.View style={{
       // flex: 1, // useful for debugging
       ...StyleSheet.absoluteFillObject,
       borderColor: 'purple',
       // borderWidth: 2,
       transform: [
           {
               translateX: animatePosition.interpolate({
                   inputRange: [-1, 0, 1],
                   outputRange: [-width, 0, width]
               })
           }
       ]
   }}>
       {children}
   </Animated.View>
)

const ChildLayer = ({ children }) => (
    <View style={{
        //flex: 1, // useful for debugging
        borderColor: 'blue',
        // borderWidth: 2,
        ...StyleSheet.absoluteFillObject
    }}>
        {children}
    </View>
)

export default class Transitions extends React.Component {
    static propTypes = {
        children: PropTypes.node,
        location: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
        direction: PropTypes.oneOf(['left', 'right'])
    }

    static defaultProps = {
        direction: 'right'
    }

    constructor (props) {
        super(props)
        // Used to animate the position of the transition layer
        this.animatePosition = new Animated.Value(1)
    }

    state = {
        previousChildren: null
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location !== this.props.location) {
            this.animate(this.props.direction)
        }
    }

    animate(directionString) {
        let from // where the animation start
        switch (directionString) {
            case 'left':
                from = -1
                break;
            case 'right':
            default:
                from = 1
                break
        }
        // We're moving!
        this.animatePosition.setValue(from)
        this.setState({ previousChildren: this.props.children })
        Animated.timing(this.animatePosition, {
            toValue: 0,
            duration: 250,
            useNativeDriver: true,
            ease: Easing.quad
        }).start(
            () => {
                this.setState({ previousChildren: null })
                this.animatePosition.setValue(-1)
                
            }
        )    
    }

    // Renders the actual children
    render () {
        const { width } = Dimensions.get('window')
        const { previousChildren } = this.state
        const { children } = this.props
        return (
            <View style={{ ...StyleSheet.absoluteFillObject }}>
                <ChildLayer>{previousChildren || children}</ChildLayer>
                <TransitionLayer
                    animatePosition={this.animatePosition}
                    width={width}
                >
                    {children}
                </TransitionLayer>

            </View>
        );
    }
}