import React from 'react';
import PropTypes from 'prop-types'; // 15.6.0

import { Text, View, StatusBar } from 'react-native';
import { Link } from 'react-router-native'; // 4.2.0
import Icons, { MaterialIcons as Icon } from '@expo/vector-icons';

export default class Navigation extends React.Component {
  static propTypes = {
    showNavBar: PropTypes.bool,
    showTabs: PropTypes.bool,
  };
  render() {
    return (
      <View
        style={{
          height: 60,
          backgroundColor: '#eeeeee',
          justifyContent: 'center',
          // alignItems: 'center',
          paddingTop: 20,
          // padding: 10,
        }}>
        <StatusBar backgroundColor="#eeeeee" barStyle="dark-content" />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderColor: 'red',
            padding: 5,
            // borderWidth: 1,
          }}>
          <Link to="/pink">
            <Icon name="directions-run" size={28} color="pink" />
          </Link>
          <Link to="/orange">
            <Icon name="directions-run" size={28} color="orange" />
          </Link>
          <Link to="/blue">
            <Icon name="directions-run" size={28} color="blue" />
          </Link>
        </View>
      </View>
    );
  }
}
