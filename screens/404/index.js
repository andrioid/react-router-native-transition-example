import React from 'react';

import { Text, View } from 'react-native';

import Navigation from '../../components/navigation/index.js'; // ERROR. The Expo team has been notified.

export default class DefaultScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Navigation />
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>404 {this.props.location.pathname} </Text>
        </View>
      </View>
    );
  }
}
