import React from 'react';

import { Text, View } from 'react-native';

import Navigation from '../../components/navigation/index.js'; // ERROR. The Expo team has been notified.

export default class MainScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Navigation />
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Hello</Text>
        </View>
      </View>
    );
  }
}
